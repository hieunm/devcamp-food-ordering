"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Tổng số bản ghi của hệ thống
var gTotalOrders = 0; //lát gán sau

// Tham số limit (Số lượng bản ghi tối đa trên 1 trang)
var gPerpage = 2;

//Khởi tạo biến global chứa thông tin items
//Kiểm tra "products" tồn tại chưa, nếu rồi thì parse ra và gán còn chưa thì tạo mới mảng rỗng
var gProductsArray = localStorage.getItem("products")
  ? JSON.parse(localStorage.getItem("products"))
  : [];
//Lưu lại giá trị cho products vào trong localstorage
localStorage.setItem("products", JSON.stringify(gProductsArray));

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  // Thực hiện xử lý hiển thị của trang đầu tiên
  // Các trang tiếp theo gán onclick trong nút phân trang và gọi tương tự trang đầu tiên
  createPage(1);

  // Lắng nghe sự kiện filter
  $(function () {
    //Event handling
    //Lắng nghe sự kiện slider(trượt thanh chọn)
    $("#slider-range").slider({
      range: true, //Thiết lập thanh trượt là một thanh trượt khoảng, cho phép chọn một khoảng giá trị
      values: [10, 30], //Thiết lập giá trị khởi đầu của thanh trượt là một khoảng từ 10 đến 30
      min: 10,
      max: 30,
      step: 0.1, //bước để nhỏ vừa thôi ko server ko trả về kịp, lag
      slide: function (event, ui) {
        //event là một đối tượng chứa thông tin về sự kiện di chuyển thanh trượt
        //ui là một đối tượng chứa thông tin liên quan đến trạng thái của thanh trượt sau sự kiện
        // /Trong trường hợp của thanh trượt với range: true, ui thường chứa một thuộc tính values là một mảng các giá trị của các "handle" trên thanh trượt.
        $("#min-price").html(ui.values[0]); //values ở đây có 2 phần tử nên ui.values[0] là lấy và gán phần tử đầu tiên(min)

        $("#max-price").html(ui.values[1]); //lấy và gán phần tử max

        createFilterPage(1); //gọi hàm filter với tham số đầu tiên là trang 1
      },
    });
    //Lắng nghe sự kiện change của các ô checkbox
    $(".Kitchen-Menu-Content .checkbox-container input[type='checkbox']").on(
      "change",
      function () {
        createFilterPage(1); // Gọi hàm filter với tham số đầu tiên là trang 1
      }
    );
  });

  // Kích hoạt tooltip
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
  });

  // Lắng nghe sự kiện ấn nút thêm vào giỏ hàng
  $("#product-container").on("click", ".add-to-cart", function () {
    onIconAddProductToCartClick(this);
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm xử lý tạo trang(dùng async await)
async function createPage(paramPagenum) {
  "use strict";
  $("#spinner-pagination").removeClass("d-none"); //Hiển thị spinner
  try {
    //thực hiện khối mã, nếu có lỗi nhảy ngay sang catch(khác với then chỉ thực hiện khi promise thành công)
    //nên dùng try để xứ lý, đặc biệt khi có nhiều promise
    //then ngắn gọn chỉ nên dùng khi có ít promise cần xử lý
    // Gọi và chờ hàm call API get orders và xử lý hiển thị dựa vào 2 tham số phân trang
    await callAPIGetOrdersPagination(gPerpage, paramPagenum);
    let isFilter = false; //tạo biến trạng thái để theo dõi trạng thái filter
    // Gọi hàm tạo thanh phân trang
    createPagination(paramPagenum, isFilter);
    $("#spinner-pagination").addClass("d-none"); //Ẩn spinner
  } catch {
    console.log(paramError);
    $("#spinner-pagination").addClass("d-none"); //Ẩn spinner
  }
}

// Hàm xử lý sự kiện filter
async function createFilterPage(paramPagenum) {
  "use strict";
  $("#spinner-pagination").removeClass("d-none"); //Hiển thị spinner
  //B0: Khai báo đối tượng
  let vQueryParamsObj = {
    _limit: gPerpage,
    _page: paramPagenum - 1,
    priceMin: 10,
    priceMax: 30,
    rating: [],
  };
  //B1: Thu thập dữ liệu
  collectDataFilter(vQueryParamsObj);
  //B2: Validate(Ko cần)
  //B3: Call API và xử lý hiển thị
  try {
    await callAPIGetOrdersPaginationWithFilter(vQueryParamsObj);
    let isFilter = true; //tạo biến trạng thái để theo dõi trạng thái filter
    // Gọi hàm tạo thanh phân trang
    createPagination(paramPagenum, isFilter);
    $("#spinner-pagination").addClass("d-none"); //Ẩn spinner
  } catch {
    console.log(paramError);
    $("#spinner-pagination").addClass("d-none"); //Ẩn spinner
  }
}

// Hàm xử lý sự kiện ấn icon add product to cart
function onIconAddProductToCartClick(paramIcon) {
  "use strict";
  // B0: Khai báo đối tượng lưu dữ liệu để add vào giỏ hàng
  // Khai báo đối tượng trong scope hàm để tránh chia sẻ dữ liệu mỗi lần add thêm thông tin vào localStorage
  // Chỉ nên khai báo global mới những biến, đối tượng có thể tái sử dụng nhiều lần mà ko sợ nhầm lẫn
  let vProductObj = {
    id: "",
    name: "",
    imageUrl: "",
    price: "",
    rating: "",
    time: "",
    createdAt: "",
    updatedAt: "",
    qty: 1,
  };
  //B1: Thu thập dữ liệu
  collectDataAddProductToCart(paramIcon, vProductObj);
  //B2: Kiếm tra dữ liệu
  let vCheck = validateDataAddProductToCart(vProductObj);
  if (vCheck) {
    //B3: Xử lý front-end
    addDataAddProductToCartToLocalStorage(vProductObj);
    // Gọi ở đây để khi bấm thêm cái là đổi màu ngay
    changeColorIconBag();
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm call API get orders và xử lý hiển thị dựa vào 2 tham số phân trang
function callAPIGetOrdersPagination(paramPerpage, paramPagenum) {
  "use strict";
  const vQueryParams = new URLSearchParams({
    _limit: paramPerpage,
    _page: paramPagenum - 1,
  });
  //Dùng Promise xử lý bất đồng bộ(ko dùng when Jquery vì ko muốn phải truyền thêm tham số Defferer)
  return new Promise((resolve, reject) => {
    $.ajax({
      type: "get",
      url:
        "https://food-ordering-fvo9.onrender.com/api/foods?" +
        vQueryParams.toString(),
      dataType: "json",
      success: function (paramData) {
        console.log(paramData);
        displayDataGetOrdersPagination(paramData); //Gọi hàm xử lý hiển thị lấy danh sách Orders phân trang
        resolve(paramData); //Trả về resolve
      },
      error: function (paramError) {
        console.log(paramError);
        reject(paramError); //Trả về reject
      },
    });
  });
}

// Hàm call API lấy danh sách Orders phân trang có bộ lọc
function callAPIGetOrdersPaginationWithFilter(paramQueryObj) {
  "use strict";
  const vQueryParams = new URLSearchParams();

  vQueryParams.append("_limit", paramQueryObj._limit);
  vQueryParams.append("_page", paramQueryObj._page);
  vQueryParams.append("priceMin", paramQueryObj.priceMin);
  vQueryParams.append("priceMax", paramQueryObj.priceMax);
  if (paramQueryObj.rating.length > 0) {
    paramQueryObj.rating.forEach((paramRating) =>
      vQueryParams.append("rating", paramRating)
    );
  }

  return new Promise((resolve, reject) => {
    $.ajax({
      type: "get",
      url:
        "https://food-ordering-fvo9.onrender.com/api/foods?" +
        vQueryParams.toString(),
      dataType: "json",
      success: function (paramData) {
        console.log(paramData);
        displayDataGetOrdersPagination(paramData); //Gọi hàm xử lý hiển thị lấy danh sách Orders phân trang
        resolve(paramData); //Trả về resolve
      },
      error: function (paramError) {
        console.log(paramError);
        reject(paramError);
      },
    });
  });
}

// Hàm thu thập dữ liệu filter
function collectDataFilter(paramQueryObj) {
  "use strict";
  //Lấy giá trị price
  paramQueryObj.priceMin = Number($("#min-price").html());
  paramQueryObj.priceMax = Number($("#max-price").html());
  // Truy vấn các ô checkbox để xem ô nào được chọn
  //Truy vấn và lấy mảng các checkbox(dùng to Array để chuyển thành mảng)
  let vCheckboxesArr = $(
    ".Kitchen-Menu-Content .checkbox-container input[type='checkbox']"
  ).toArray();
  vCheckboxesArr.forEach((paramCheckbox) => {
    if (paramCheckbox.checked) {
      //nếu checkbox đc chọn
      let vRating = Number($(paramCheckbox).val()); //lấy val của nó
      paramQueryObj.rating.push(vRating); //thêm vào mảng rating
    }
  });
}

// Hàm xử lý hiển thị lấy danh sách Orders phân trang
function displayDataGetOrdersPagination(paramData) {
  "use strict";
  gTotalOrders = paramData.count; //Gán giá trị cho biến toàn cục tổng số order lấy đc
  //Xóa trắng danh sách cũ
  $("#product-container").html("");
  //Dùng Object.values đổi giá trị order trả về thành 1 mảng để dùng forEach
  let vOrdersArr = Object.values(paramData.rows);
  console.log(vOrdersArr);
  if (vOrdersArr.length > 0) {
    //Nếu có sản phẩm lọc phù hợp
    //Chạy vòng lặp xử lý hiển thị với data trả về
    vOrdersArr.forEach((paramOrder) => {
      //Với mỗi order sẽ tiến hành tạo 1 phần tử con để hiển thị
      $("#product-container").append(`
        <div class="Product-card col-12 col-md-6 col-lg-4 mb-4">
        <div class="product-card-image" style="background-image: url('${paramOrder.imageUrl}'); background-position: center; background-size: cover; background-repeat: no-repeat;">
          <div class="discount">50%</div>
        </div>
        <div class="product-id d-none">${paramOrder.id}</div>
        <div class="product-imageUrl d-none">
        ${paramOrder.imageUrl}
        </div>
        <div class="product-createdAt d-none">
        ${paramOrder.createdAt}
        </div>
        <div class="product-updatedAt d-none">
        ${paramOrder.updatedAt}
        </div>
        <div class="product-card-details">
          <div class="product-card-details-content">
            <div class="row">
              <div class="product-name" data-toggle="tooltip" data-placement="bottom" title="${paramOrder.name}">${paramOrder.name}</div>
              <div class="product-price">
                $<span>${paramOrder.price}</span>
              </div>
            </div>
            <div class="row">
              <div>
                <div class="card-rating">
                  <img
                    src="images/ant-design_star-filled.png"
                    alt="rating star"
                  />
                  <div class="card-rating-number">
                  ${paramOrder.rating}
                  </div>
                </div>
                <div class="card-cooking-time">
                ${paramOrder.time}
                </div>
              </div>
              <img
                src="images/iconoir_add-to-cart.png"
                alt="add-to-cart"
                class="add-to-cart btn"
              />
            </div>
          </div>
        </div>
      </div>
        `);
    });
  } else {
    $("#product-container").html("Không tìm thấy sản phẩm phù hợp");
  }
}

// Hàm xử lý tạo thanh phân trang
function createPagination(paramPagenum, isFilter) {
  "use strict";
  // Xóa trắng phần tử chứa thanh phân trang cũ
  $("#pagination-container").html("");
  // Tổng số trang. Math ceil để lấy số sản phẩm tối đa có được. Ví dụ: 10 / 3 = 3.33 => Cần 4 trang hiển thị
  // Khai báo ở đây để chờ gTotalOrders được gán giá trị xong
  let vTotalPages = Math.ceil(gTotalOrders / gPerpage);

  // Mảng chứa các trang cần hiển thị
  // Do bài này ko đặt nút prev next nên hiển thị toàn bộ số trang
  let vPagesToDisplayArr = calculateAllPagesToDisplay(vTotalPages);

  // Lặp qua mảng chứa các trang cần hiển thị
  for (let bI = 0; bI < vPagesToDisplayArr.length; bI++) {
    let vPageNumber = vPagesToDisplayArr[bI]; //Biến chứa số trang
    //Khai báo biến trạng thái để kiểm tra trang hiện tại
    let isActive = vPageNumber === paramPagenum; // nhận giá trị true false

    // Thêm nút vào thanh phân trang
    appendPaginationItem(vPageNumber, isActive, isFilter);
  }
}

// Hàm xử lý tính toán số trang cần hiển thị
function calculateAllPagesToDisplay(paramTotalPageCount) {
  "use strict";
  let vPagesToDisplay = []; //Mảng rỗng dùng để lưu số trang cần hiển thị

  for (let bI = 1; bI <= paramTotalPageCount; bI++) {
    //lặp qua các số từ 1 đến giá trị của biến paramTotalPageCount(bắt đầu từ trang 1)
    vPagesToDisplay.push(bI); //thêm số trang vào mảng
  }

  return vPagesToDisplay;
}

// Hàm xử lý thêm nút vào thanh phân trang
function appendPaginationItem(paramPageNumber, paramIsActive, isFilter) {
  "use strict";
  let vListItem = $("<li class='page-item'></li>"); //Khai báo thẻ li mặc định bao ngoài nút phân trang

  if (paramIsActive) {
    //Nếu trang truyền vào đc chọn(active)
    vListItem.addClass("active"); //thêm class active vào thẻ li
    vListItem.append(
      "<a href='javascript:void(0)' class='page-link' style='background-color: #1AC073; border-color: #1AC073'>" +
        paramPageNumber +
        "</a>"
    ); //thêm nút với css active
  } else {
    if (!isFilter) {
      //Nếu ko lọc thì gán sự kiện tạo mới page
      vListItem.append(
        "<a href='javascript:void(0)' class='page-link' onclick='createPage(" +
          paramPageNumber +
          ")'>" +
          paramPageNumber +
          "</a>"
      ); //thêm nút với css mặc định
    } else {
      //Nếu đang lọc thì gán sự kiện tạo mới filter page
      vListItem.append(
        "<a href='javascript:void(0)' class='page-link' onclick='createFilterPage(" +
          paramPageNumber +
          ")'>" +
          paramPageNumber +
          "</a>"
      ); //thêm nút với css mặc định
    }
  }

  $("#pagination-container").append(vListItem); //add thẻ li mới vào thanh phân trang
}

// Hàm xử lý thu thập dữ liệu sản phẩm khi ấn add to cart
function collectDataAddProductToCart(paramIcon, paramProductObj) {
  "use strict";
  let vCard = $(paramIcon).closest(".Product-card"); //Tìm đến thẻ cha Product-card gần nhất
  //gán dữ liệu cho object
  paramProductObj.id = vCard.find(".product-id").html();
  paramProductObj.imageUrl = vCard.find(".product-imageUrl").html();
  paramProductObj.name = vCard.find(".product-name").html();
  paramProductObj.price = vCard.find(".product-price").find("span").html();
  paramProductObj.rating = vCard.find(".card-rating-number").html();
  paramProductObj.time = vCard.find(".card-cooking-time").html();
  paramProductObj.createdAt = vCard.find(".product-createdAt").html();
  paramProductObj.updatedAt = vCard.find(".product-updatedAt").html();
  //chạy vòng lặp while xem gProductsArray đã có sản phẩm vừa thêm chưa
  //nếu có rồi thì +1 thêm vào qty mới và xóa cái cũ đi, nếu chưa có thì sử dụng mặc định ban đầu là 1
  let vFound = false;
  let vIndex = 0;
  while (!vFound && vIndex < gProductsArray.length) {
    if (paramProductObj.id == gProductsArray[vIndex].id) {
      paramProductObj.qty += gProductsArray[vIndex].qty; //cộng thêm 1 vào qty
      gProductsArray.splice(vIndex, 1); //xóa đối tượng cũ đi khỏi mảng
      vFound = true;
    } else {
      vIndex++;
    }
  }
}

// Hàm xử lý kiểm tra dữ liệu sản phẩm khi ấn add to cart
function validateDataAddProductToCart(paramProductObj) {
  "use strict";
  if (paramProductObj.id == "Viết Id pizza vào đây") {
    alert("Không tìm thấy sản phẩm tương ứng");
    return false;
  }
  if (paramProductObj.imageUrl == "Viết imageUrl pizza vào đây") {
    alert("Không tìm thấy sản phẩm tương ứng");
    return false;
  }
  if (paramProductObj.name == "Viết tên pizza vào đây") {
    alert("Không tìm thấy sản phẩm tương ứng");
    return false;
  }
  if (paramProductObj.price == "Viết giá tiền pizza vào đây") {
    alert("Không tìm thấy sản phẩm tương ứng");
    return false;
  }
  if (paramProductObj.rating == "Viết số đánh giá sao của khách hàng vào đây") {
    alert("Không tìm thấy sản phẩm tương ứng");
    return false;
  }
  if (paramProductObj.time == "Viết độ dài chờ để nướng bánh vào đây") {
    alert("Không tìm thấy sản phẩm tương ứng");
    return false;
  }
  if (paramProductObj.createdAt == "Viết createdAt pizza vào đây") {
    alert("Không tìm thấy sản phẩm tương ứng");
    return false;
  }
  if (paramProductObj.updatedAt == "Viết updatedAt pizza vào đây") {
    alert("Không tìm thấy sản phẩm tương ứng");
    return false;
  }
  return true;
}

// Hàm xử lý front-end thêm dữ liệu product đã chọn vào localStorage
function addDataAddProductToCartToLocalStorage(paramProductObj) {
  "use strict";
  alert("Thêm sản phẩm vào giỏ hàng thành công");
  // Đính thêm đối tượng chứ thông tin product được chọn vào mảng chứa thông tin giỏ hàng
  gProductsArray.push(paramProductObj);
  // Gán lại giá trị mảng chứa thông tin giỏ hàng trong LocalStorage
  localStorage.setItem("products", JSON.stringify(gProductsArray));
}
