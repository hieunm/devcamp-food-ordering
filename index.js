//khai báo thư viện, app và port
const express = require('express');
const path = require('path');
const app = express();
const port = 8000;

//định nghĩa middleware static cho thư mục views
app.use(express.static('views'));

//định nghĩa route cho trang homepage
app.get('/', (req, res) => {
    const indexPath = path.join(__dirname, './views/HomePage.html');
    res.sendFile(indexPath);
})

//định nghĩa route cho trang menulist
app.get('/card', (req, res) => {
    const indexPath = path.join(__dirname, './views/MenuList.html');
    res.sendFile(indexPath);
})

//định nghĩa route cho trang giỏ hàng
app.get('/foods', (req, res) => {
    const indexPath = path.join(__dirname, './views/MyCart.html');
    res.sendFile(indexPath);
})

//định nghĩa route cho trang admin
app.get('/admin', (req, res) => {
    const indexPath = path.join(__dirname, './views/Admin/index.html');
    res.sendFile(indexPath);
})

//khởi tạo server
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
})